// This code is for testing the passive buzzer
// https://www.sunfounder.com/learn/sensor_kit_v1_for_Arduino/lesson-12-buzzer-sensor-kit-v1-for-arduino.html

int buzzerPin = 13; // D13 in Arduino Nano, D7 in NodeMCU

void setup() {                                                                    
  pinMode(buzzerPin, OUTPUT);
}

void loop() {
  for (int i = 0; i < 80; i++) {  // make a sound
    digitalWrite(buzzerPin, HIGH); // send high signal to buzzer 
    delay(1); // delay 1ms
    digitalWrite(buzzerPin, LOW); // send low signal to buzzer
    delay(1);
  }
  delay(50);
  for (int j = 0; j < 100; j++) { //make another sound
    digitalWrite(buzzerPin, HIGH);
    delay(1); // delay 2ms
    digitalWrite(buzzerPin, LOW);
    delay(1);
  }
  delay(100); // Adding a delay of one second.

}
