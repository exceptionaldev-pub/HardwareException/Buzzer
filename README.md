https://www.sunfounder.com/learn/sensor_kit_v1_for_Arduino/lesson-12-buzzer-sensor-kit-v1-for-arduino.html

سنسوری برای تولید انواع هشدار ها


| buzzer | board |
| ------ | ------ |
| - | Ground |
| + | 5v | 
| S | 13 | 


Note: Set Processor to ATmega328P (old bootloader) on Arduino Nano

<img src="NodeMCU_pinout.png">